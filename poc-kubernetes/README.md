## Arsys tests - Kubernetes service offering description

### Reference documentation

- [ ] [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)
- [ ] [Service characteristics WG](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/tree/develop/single-point-of-truth/yaml/gax-trust-framework)
- [ ] [Kubernetes website](https://kubernetes.io/docs/concepts/)

### Notes

Services and Resources have been tried to be described using the proper type defined in the SC working group repo.

***

- soKubernetesCluster.json.json (Orchestration service): Added two attributes: addOns and configValues.

- soCloudAdvancedServer.json: "endpoint" attribute is the equivalent for "Service Access Point" defined in the trust framework.

