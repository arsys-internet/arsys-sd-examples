## Service offering description

### Reference documentation

- [ ] [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)
- [ ] [Service characteristics WG (246-location-grouping)](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/tree/246-location-groupings-region-datacenter-az-for-provider-spanning-services/single-point-of-truth/yaml/gax-trust-framework?ref_type=heads)

### Notes

Services and Resources have been tried to be described using the proper type defined in the SC working group repo.

***

- reDatacenterES.json 
  - Also added environmental impact attributes.

- soVirtualMachine.json
  - Check "instantiationConfiguration".

- storage-service-offering
  - Check name for attributes (readme and yaml file).
  - title: 'lLifetimeManagement'.

- connectivity-service-offering
  - Could NetworkServiceOffering have wrong subclass?  
