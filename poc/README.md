## Arsys tests for services and resources descriptions

### Reference documentation

- [ ] [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)
- [ ] [Data Exchange Services Specifications](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/dewg/)
- [ ] [Service characteristics WG](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/tree/develop/single-point-of-truth/yaml/gax-trust-framework)

### Notes

Services and Resources have been described using the proper type defined in the SC repo.

***

- reDatacenterEs.json (physical resource): Create a definition file for point 7.2.1 Sustainability defined at trust framework?

- reDataSpaceConnector.json (software resource): "webAddress" attribute may be useful?

- soCloudAdvancedServer.json (virtual machine): "instantiationConfiguration" instead of "instantiationReq" to define configuration values.  

- soAdditionalStorage.json (storage offering): "disk" attribute added to detail service characteristics.
