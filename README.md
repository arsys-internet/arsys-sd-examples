# Arsys 

## Gaia-X services and resources description tests

Descriptions based on the Gaia trust framework and the work from the service characteristics working group.  

- [ ] [Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)
- [ ] [Service characteristics WG](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/tree/develop/single-point-of-truth/yaml/gax-trust-framework)

***

### 2023-07-21

- participant folder: Testing verifiable Credential (Participant) including "legal registration number" and "terms and conditions" updates.

### 2023-07-13

- poc-kubernetes folder: Kubernetes infrastructure description.  

### 2023-07-13

- poc: Data Space service offering description.
